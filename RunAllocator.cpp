// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <vector>
#include <utility>
#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */

    int num_tests = 0;
    cin >> num_tests;
    string line;

    getline(cin, line);
    getline(cin, line);
    vector<pair<double *, size_t>> allocations;

    int i = 0;
    while (i < num_tests) {
        my_allocator<double, 1000> alloc;
        int req = 0;

        getline(cin, line);
        while (line.compare("") != 0) {
            req = stoi(line);

            if (req >= 0) {
                int place = 0;
                pair<double *, size_t> p = make_pair(alloc.allocate(req), (size_t) req);

                for (pair<double *, size_t> ap : allocations) {
                    if (p.first < ap.first)
                        break;
                    place++;
                }

                allocations.insert(allocations.begin() + place, p);

            } else {
                req = abs(req) - 1;
                alloc.deallocate(allocations.at(req).first, req);
                allocations.erase(allocations.begin() + req);
            }

            getline(cin, line);


        }

        my_allocator<double, 1000>::iterator b = alloc.begin();
        my_allocator<double, 1000>::iterator e = alloc.end();

        cout << *b;
        ++b;
        while (b != e) {
            cout << " " << *b;
            ++b;
        }
        cout << endl;

        allocations.clear();
        i++;
    }

    return 0;
}
