// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator
#include <vector>

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1,);
#else
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);
#endif

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2,);
#else
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);
#endif

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;

    const allocator_type x;
    ASSERT_EQ(x[100 - sizeof(allocator_type)], 92);
}                                         // fix test

/* TESTS ADDED BY ME */

TYPED_TEST(AllocatorFixture2, test4) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;

    allocator_type x;
    size_type s = 100 / sizeof(value_type) + 1;
    ASSERT_THROW(x.allocate(s), std::bad_alloc);
}

TYPED_TEST(AllocatorFixture2, test5) {
    using allocator_type = typename TestFixture::allocator_type;
    using size_type      = typename TestFixture::size_type;

    allocator_type x;
    size_type s = 0;
    ASSERT_THROW(x.allocate(s), std::bad_alloc);
}

TYPED_TEST(AllocatorFixture2, test6) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;

    allocator_type x;
    size_type s;
    if (100 % sizeof(value_type) == 0)
        s = 100 / sizeof(value_type) - 2;   //case for the int type
    else
        s = 100 / sizeof(value_type) - 1;   //case for the double type

    x.allocate(s);
    ASSERT_EQ(x.allocate(1), nullptr);
}

TYPED_TEST(AllocatorFixture2, test7) {
    using allocator_type = typename TestFixture::allocator_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;
    size_type s = 1;
    pointer p = x.allocate(s);

    x.deallocate(p, 0);
    ASSERT_THROW(x.deallocate(p, 0), std::invalid_argument);
}

TYPED_TEST(AllocatorFixture2, test8) {
    using allocator_type = typename TestFixture::allocator_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;
    size_type s = 1;
    pointer p = x.allocate(s);
    p = (pointer) nullptr;

    ASSERT_THROW(x.deallocate(p, 0), std::invalid_argument);
}

TYPED_TEST(AllocatorFixture2, test9) {
    using allocator_type = typename TestFixture::allocator_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;
    size_type s = 1;
    pointer p = x.allocate(s);
    p = (pointer) 0xFFFFFFFFFFFFFFFF;   //max x86_64 address

    ASSERT_THROW(x.deallocate(p, 0), std::invalid_argument);
}

TEST(AllocatorIterator, test0) {
    using namespace std;

    my_allocator<int, 100> x;
    vector<int *> v;
    size_t expected[4] = {-1 * sizeof(int), -2 * sizeof(int), -3 * sizeof(int), 44};
    for (int i = 0; i < 3; ++i)
        v.push_back(x.allocate(i + 1));

    my_allocator<int, 100>::iterator b = x.begin();
    my_allocator<int, 100>::iterator e = x.end();

    int i = 0;
    while (b != e) {
        ASSERT_EQ(*b, expected[i]);
        ++b;
        ++i;
    }

}

TEST(AllocatorIterator, test1) {

    my_allocator<int, 100> x;
    my_allocator<int, 100>::iterator b = x.begin();
    my_allocator<int, 100>::iterator e = x.end();

    ASSERT_NE(*e, *b);

}

TEST(AllocatorIterator, test2) {

    my_allocator<int, 100> x;
    my_allocator<int, 100>::iterator b = x.begin();
    my_allocator<int, 100>::iterator e = x.end();
    ++b;
    ASSERT_EQ(*e, *b);

}

TEST(AllocatorIterator, test3) {

    my_allocator<int, 100> x;
    my_allocator<int, 100>::iterator b = x.begin();
    my_allocator<int, 100>::iterator e = x.end();
    x.allocate(1);
    ++b;
    --b;
    ++b;
    ASSERT_EQ(*e, *b);

}