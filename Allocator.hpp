// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * Checks equality of two iterators
         * @param lhs - first iterator to check
         * @param rhs - second iterator to check
         * @return Returns whether the two iterators point to the same block of the same my_allocator object
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        /**
         * Checks equality of two iterators
         * @param lhs - first iterator to check
         * @param rhs - second iterator to check
         * @return Returns the opposite of the == operator
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        /**
         *  Constructor for an iterator over the blocks of a my_allocator object
         * @param p - an int pointer to the location of the beginning of the first or last block of a my_alloctor object
         */
        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        /**
         * Returns the value of the block size of the memory in the block this iterator points to
         * @return The a reference to the size of the memory in the block this iterator points to
         */
        int& operator * () const {
            // <your code>
            return *_p;
        }           // replace!

        // -----------
        // operator ++
        // -----------

        /**
         * Increments this iterator to the next block in the allocator
         * @return Returns a reference to this iterator with an incremented value
         */
        iterator& operator ++ () {
            // <your code>
            _p += abs(*_p) / sizeof(int) + 2;
            return *this;
        }

        // -----------
        // operator ++
        // -----------


        /**
         * Increments this iterator to the next block in the allocator
         * @param int - iterator to increment
         * @return Returns this iterator with an incremented value
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrements this iterator to the previous block in the allocator
         * @return Returns a reference to this iterator with a decremented value
         */
        iterator& operator -- () {
            // <your code>
            _p -= abs(*_p) / sizeof(int) - 2; //go to the beginning of the previous block, *p bytes / sizeof int
            return *this;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrements this iterator to the previous block in the allocator
         * @param int - iterator to decrement
         * @return Returns a copy of this iterator with a decremented value
         */
        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * Checks equality of two const_iterators
         * @param lhs - first const_iterator to check
         * @param rhs - second const_iterator to check
         * @return Returns whether the two const_iterators point to the same block of the same my_allocator object
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        /**
         * Checks equality of two const_iterators
         * @param lhs - first const_iterator to check
         * @param rhs - second const_iterator to check
         * @return Returns the opposite of the == operator
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        /**
         *  Constructor for a const_iterator over the blocks of a my_allocator object
         * @param p - a constant int pointer to he location of the beginning of the first or last block of a my_alloctor object
         */
        const_iterator (const int* p) : _p(p) {}

        // ----------
        // operator *
        // ----------

        /**
         * Returns the value of the block size of the memory in the block this const_iterator points to
         * @return The an immutable reference to the size of the memory in the block this const_iterator points to
         */
        const int& operator * () const {
            // <your code>
            return *_p;
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        /**
         * Increments this const_iterator to the next block in the allocator
         * @return Returns a reference to this const_iterator with an incremented value
         */
        const_iterator& operator ++ () {
            // <your code>
            _p += abs(*_p) / sizeof(int) + 2;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * Increments this const_iterator to the next block in the allocator
         * @param int - const_iterator to increment
         * @return Returns a copy of this const_iterator with an incremented value
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrements this const_iterator to the previous block in the allocator
         * @return Returns a reference to this const_iterator with a decremented value
         */
        const_iterator& operator -- () {
            // <your code>
            _p -= abs(*_p) / sizeof(int) - 2; //go to the beginning of the previous block, *p bytes / sizeof int
            return *this;
        }

        // -----------
        // operator --
        // -----------

        /**
         * Decrements this const_iterator to the previous block in the allocator
         * @param int - const_iterator to decrement
         * @return Returns a copy of this const_iterator with a decremented value
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     * An invalid heap would be where:
     *      - Two positive sentinels are adjacent
     *      - The size of the sum of the absolute value of the sentinels
     *        is greater than the size of the heap
     *      - There are no sentinels
     *      - There is no sentinel for a section of memory (sentinel sum != size of heap)
     *      - A sentinel is 0
     * @return Returns whether this allocator is in a valid state
     */
    bool valid () const {
        // <your code>
        // <use iterators>
        const_iterator b = begin();
        const_iterator e = end();

        int sum = 0;
        int last_sign = 0;

        while (b != e) {
            if ((*b > 0 && last_sign > 0)) //two positive blocks adjacent
                return false;

            if (*b == 0)
                return false;

            sum += abs(*b) + (2 * sizeof(int));

            if (*b < 0)
                last_sign = -1;
            else
                last_sign = 1;

            ++b;
        }

        if (sum != N)   //sum of the sentinels (including their size) should be == N
            return false;

        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int)))
            throw std::bad_alloc();

        (*this)[0] = N - (2 * sizeof(int));
        (*this)[N - sizeof(int)] = N - (2 * sizeof(int));
        ;
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     *
     * Allocates a block of memory of at least s * (allocator value_type) size
     * @param s - The amount of memory to allocate
     * @return A pointer to a block of memory of at least size s * (allocator value_type)
     */
    pointer allocate (size_type s) {
        // <your code>
        assert(valid());

        int alloc_size = (s * sizeof(T)) + (2 * sizeof(int));

        if (alloc_size < (int) (sizeof(T) + (2 * sizeof(int))))
            throw std::bad_alloc();

        if ((s * sizeof(T)) > (N - (2 * sizeof(int)))) //throw a bad_alloc if size is greater than heap size
            throw std::bad_alloc();

        int *b = (int *) &a[0];
        int *e = (int *) &a[N];
        int *left_b_end;
        int *right_b_end;
        int *right_b_start;

        while (b < e) {
            if (*b >= (int) (alloc_size + sizeof(T))) {      //if b is big enough for this allocation and at least a minimum one, split it
                left_b_end = b + ((s * sizeof(T)) / sizeof(int)) + 1;
                right_b_end = b + (abs(*b) / sizeof(int)) + 1;
                right_b_start = left_b_end + 1;

                *right_b_end = *right_b_start = *b - alloc_size;
                *left_b_end = *b = -(s * sizeof(T));

                return ((pointer) (b + 1));

            } else if (*b >= (int) (s * sizeof(T))) {  //if b is only big enough for this allocation, give the whole block
                left_b_end = b + (*b / sizeof(int)) + 1;
                *left_b_end = *b = -(*b);

                return ((pointer) (b + 1));
            }

            b += abs((int) (*b / sizeof(int))) + 2; //move to the next block
        }

        return nullptr;
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     *
     * Deallocates a block of memory at pointer p obtained from my_allocator
     * @param p - the pointer to memory returned by my_allocator
     * @param n - unused
     */
    void deallocate (pointer p, size_type n) {
        // <your code>

        //check to see if p is valid
        int *block = ((int *) p) - 1;

        if (block < (int *) &a[0] || block >= (int *) &a[N])    //the minus 1 is to make sure block isn't the last sentinel
            throw std::invalid_argument("p must be in the heap");

        int *block_end = block + (abs(*block) / sizeof(int)) + 1;

        if (block_end >= (int *) &a[N])    //the minus 1 is to make sure block isn't the last sentinel
            throw std::invalid_argument("p must be in the heap");

        if (*block != *block_end) {//if the beginning of the block (1st sentinel) doesn't match the end (2nd sentinel)

            throw std::invalid_argument("p isn't in a valid heap segment");
        }
        if (*block > 0)
            throw std::invalid_argument("p is already free");

        *block_end = *block = -*block;
        int *pblock_start;
        int *pblock_end = block - 1;
        int *nblock_start = block_end + 1;
        int *nblock_end;

        //deallocate block
        if (pblock_end >= (int *) &a[0]) {
            if (*pblock_end > 0) {  //coalesce this block into the previuos one if it is free
                pblock_start = pblock_end - (*pblock_end / sizeof(int)) - 1;
                *pblock_start += *block + (2 * sizeof(int));
                *block_end = *pblock_start;
                block = pblock_start;
            }

        }

        if (nblock_start < (int *) &a[N]) {
            if (*nblock_start > 0) { //coalesce this block into the next one if it is free
                nblock_end = nblock_start + (*nblock_start / sizeof(int)) + 1;
                *block += *nblock_start + (2 * sizeof(int));
                *nblock_end = *block;
            }
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
